package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {

    ShoppingCart shoppingCart = new ShoppingCart();

    @Test
    void addProduct() {
        Product mock1 = new Product("Strawberry", 20, 200);
        Product mock2 = new Product("Apple", 4, 150);
        Product mock3 = new Product("Milk", 2, 200);
        Product mock4 = new Product("Banana", -3, 0);
        Product mock5 = new Product("", -3, -2);

        assertTrue(shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity()));
        assertFalse(shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity()));
        assertFalse(shoppingCart.addProducts(mock4.getName(), mock4.getPrice(), mock4.getQuantity()));
        assertFalse(shoppingCart.addProducts(mock5.getName(), mock5.getPrice(), mock5.getQuantity()));
    }

    @Test
    void deleteProduct() {
        Product mock1 = new Product("Blueberry", 20, 200);
        Product mock2 = new Product("Peach", 4, 100);
        shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity());
        shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity());

        assertTrue(shoppingCart.deleteProducts(mock1.getName(), 150));
        assertTrue(shoppingCart.deleteProducts(mock1.getName(), 50));
        assertFalse(shoppingCart.deleteProducts(mock1.getName(), 150));
        assertFalse(shoppingCart.deleteProducts(mock2.getName(), 150));
        assertFalse(shoppingCart.deleteProducts("Raspberry", 100));
    }

    @Test
    void getQuantityOfProduct() {
        Product mock1 = new Product("Strawberry", 20, 100);
        Product mock2 = new Product("Blueberry", 20, 200);
        Product mock3 = new Product("Peach", 4, 90);
        shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity());
        shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity());
        shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity());

        assertEquals(100, shoppingCart.getQuantityOfProduct(mock1.getName()));
        assertEquals(200, shoppingCart.getQuantityOfProduct(mock2.getName()));
        assertEquals(90, shoppingCart.getQuantityOfProduct(mock3.getName()));

        shoppingCart.deleteProducts(mock1.getName(), 100);
        assertEquals(0, shoppingCart.getQuantityOfProduct(mock1.getName()));
    }

    @Test
    void getSumProductsPrices() {
        Product mock1 = new Product("Strawberry", 20, 100);
        Product mock2 = new Product("Blueberry", 20, 200);
        Product mock3 = new Product("Peach", 4, 90);
        shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity());
        shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity());
        shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity());

        assertEquals(6360, shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts(mock1.getName(), 100);
        assertEquals(4360, shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts(mock2.getName(), 200);
        shoppingCart.deleteProducts(mock3.getName(), 90);

        assertEquals(0, shoppingCart.getSumProductsPrices());
    }

    @Test
    void getProductPrice() {
        Product mock1 = new Product("Strawberry", 20, 100);
        Product mock2 = new Product("Peach", 4, 90);
        shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity());
        shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity());

        assertEquals(20, shoppingCart.getProductPrice(mock1.getName()));
        assertEquals(4, shoppingCart.getProductPrice(mock2.getName()));
        assertEquals(0, shoppingCart.getProductPrice("Blueberry"));
    }

    @Test
    void getProductsNames() {
        Product mock1 = new Product("Strawberry", 20, 20);
        Product mock2 = new Product("Apple", 4, 15);
        Product mock3 = new Product("Milk", 2, 20);
        Product mock4 = new Product("Blueberry", 20, 2);
        Product mock5 = new Product("Peach", 4, 9);
        shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity());
        shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity());
        shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity());
        shoppingCart.addProducts(mock4.getName(), mock4.getPrice(), mock4.getQuantity());
        shoppingCart.addProducts(mock5.getName(), mock5.getPrice(), mock5.getQuantity());

        List<String> list = new ArrayList<>(Arrays.asList("Strawberry", "Apple", "Milk", "Blueberry", "Peach"));

        assertEquals(list, shoppingCart.getProductsNames());
    }
}
