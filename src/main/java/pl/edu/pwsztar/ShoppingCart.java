package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    List<Product> products = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {

        int quantityOfProducts = products.stream().mapToInt(Product::getQuantity).sum();

        if(quantityOfProducts + quantity > PRODUCTS_LIMIT || quantity <= 0 || price <= 0 || productName.equals("")) {
            return false;
        }

        if(products.stream().anyMatch(product -> product.getName().equals(productName))) {
            Product product = products.stream().filter(prod ->
                    prod.getName().equals(productName)).collect(Collectors.toList()).get(0);
            if(product.getPrice() == price) {
                product.addQuantity(quantity);
                return true;
            } else {
                return false;
            }
        }
        products.add(new Product(productName, price, quantity));
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {

        if((quantity > 0 || !productName.equals("")) &&
                products.stream().anyMatch(product -> product.getName().equals(productName))) {
            Product product = products.stream().filter(prod ->
                    prod.getName().equals(productName)).collect(Collectors.toList()).get(0);
            if(product.getQuantity() > quantity) {
                product.subtractQuantity(quantity);
                return true;
            } else if(product.getQuantity() == quantity) {
                products.remove(product);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if(products.stream().anyMatch(product -> product.getName().equals(productName))) {
            Product product = products.stream().filter(prod ->
                    prod.getName().equals(productName)).collect(Collectors.toList()).get(0);
            return product.getQuantity();
        }
        return 0;
    }

    public int getSumProductsPrices() {
        return products.stream().mapToInt(product ->
                product.getQuantity() * product.getPrice()).sum();
    }

    public int getProductPrice(String productName) {
        if(products.stream().anyMatch(product -> product.getName().equals(productName))) {
            Product product = products.stream().filter(prod ->
                    prod.getName().equals(productName)).collect(Collectors.toList()).get(0);
            return product.getPrice();
        }
        return 0;
    }

    public List<String> getProductsNames() {
        return products.stream().collect(
                ArrayList::new,
                (strings, product) -> {
                    strings.add(product.getName());
                },
                ArrayList::addAll
                );
    }
}
